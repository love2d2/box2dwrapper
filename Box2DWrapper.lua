--[[
==================================================================================
  AUTHOR                            : Damien ETHÈVE
  DESCRIPTION                       : A box 2D wrapper for love2D framework 
  LANGUAGE                          : lua
  LOVE_VERSION                      : 11.2

  new(force, meter)                 : create a physics world
  updateB2(dt)                      : update the world
  drawB2()                          : draw all object in the world
  get(id)                           : get an object
  remove(id)                        : remove an object
  newBox(id, type, x, y, w, h)      : create box 
  newCircle(id, type, x, y, r)      : create circle
  newPolygon(id, type, vertice)     : create new polygon
  
  newRagdoll(id or nil,             : create new ragdoll
              x, y, mask or nil) 
    + get_f(name)                   : name can take ( head, torse, bd1, bd2, 
                                      bg1, bg2, jd1, jd2, jg1, jg2 )

    + get_j(name)                   : name can take ( j_HeadTorse, j_TorseJd1, 
                                      j_TorseJg1, j_Jd1Jd2, j_Jg1Jg2, j_TorseBd1,
                                      j_TorseBg1, j_Bd1Bd2, j_Bg1Bg2 )

    + remove_j(name)                : remove a joint

==================================================================================
]]

return {
  new = function (force, meter)
    local o = {
      world = nil,
      obj = {},
      global = {
        actualRagdollRange  = 50,  -- define a range for ragdoll 
        maxRagdollRange     = 60     
      }
    }

    function o:loadB2()
      love.physics.setMeter(meter)
      self.world = love.physics.newWorld(0, force*meter, true)
    end
    o:loadB2()

    function o:updateB2(dt)
      self.world:update(dt)
    end

    function o:drawB2()
      for i,v in ipairs(self.obj) do
        v:draw()
      end
    end

    -- methods
    function o:get(id) 
      for i,v in ipairs(self.obj) do
        if v.id == id then return v end
      end
    end

    function o:remove(id)
      for i,v in ipairs(self.obj) do
        if v.id == id then
          v.fixture:destroy()
          v.body:destroy()
          table.remove(self.obj, i )
        end
      end
    end

    function o:newBox(id, type, x, y, w, h)
      local _obj = {}
      _obj.id = id
      _obj.body = love.physics.newBody(self.world, x, y, type)
      _obj.shape = love.physics.newRectangleShape(0, 0, w, h)
      _obj.fixture = love.physics.newFixture(
        _obj.body,
        _obj.shape
      )
      _obj.fixture:setRestitution(0.5452)

      function _obj:draw()
        love.graphics.polygon(
          "fill",
          self.body:getWorldPoints(
            self.shape:getPoints()
          )
        )
      end

      table.insert(self.obj, _obj)
    end

    function o:newCircle(id, type, x, y, r)
      _obj = {}
      _obj.id = id
      _obj.body = love.physics.newBody(self.world, x, y, type)
      _obj.shape = love.physics.newCircleShape(r)
      _obj.fixture = love.physics.newFixture(
        _obj.body,
        _obj.shape
      )

      _obj.fixture:setRestitution(0.4)

      function _obj:draw()
        love.graphics.circle(
          "fill",
          self.body:getX(),
          self.body:getY(),
          self.shape:getRadius()
        )
      end

      table.insert(self.obj, _obj)
    end

    function o:newPolygon(id, type, vertice)
      local _obj = {}
      _obj.id = id
      _obj.body = love.physics.newBody(self.world, x, y, type)
      _obj.shape = love.physics.newPolygonShape(vertice)
      _obj.fixture = love.physics.newFixture(
        _obj.body,
        _obj.shape
      )

      function _obj:draw()
      love.graphics.polygon(
        "fill",
        self.body:getWorldPoints(
          self.shape:getPoints()
        )
      )
    end

      table.insert(self.obj, _obj)
    end

    function o:newRagdoll(_id, _x, _y, groupe)
      local ragdoll = {}
      ragdoll.lstJoint = {}
      ragdoll.lstBody = {}

      -- define id
      if _id == nil then
        if self.global.actualRagdollRange <= self.global.maxRagdollRange then
          ragdoll.id = self.global.actualRagdollRange
          self.global.actualRagdollRange = self.global.actualRagdollRange + 1
        else
          return nil
        end
      else
        ragdoll.id = _id
      end

      -- define groupe
      if groupe == nil then 
        ragdoll.groupe = -self.global.actualRagdollRange -- collision mask
      else
        ragdoll.groupe = groupe
      end

      -- create all fixtures
      b2:newBox(ragdoll.id..'head', 'dynamic', _x, _y, 20, 20)
      b2:newBox(ragdoll.id..'torse', 'dynamic', _x, _y + 20+10, 12, 40)
      b2:newBox(ragdoll.id..'bd1', 'dynamic', _x, _y + 10+10, 8, 20)
      b2:newBox(ragdoll.id..'bd2', 'dynamic', _x, _y + 20+ 10+5, 7, 20)
      b2:newBox(ragdoll.id..'bg1', 'dynamic', _x, _y + 10+10, 8, 20)
      b2:newBox(ragdoll.id..'bg2', 'dynamic', _x, _y + 20+ 10+5, 7, 20)
      b2:newBox(ragdoll.id..'jd1', 'dynamic', _x, _y + 20+10+10+20, 9, 20)
      b2:newBox(ragdoll.id..'jd2', 'dynamic', _x, _y + 20+10+10+20+10+10, 8, 20)
      b2:newBox(ragdoll.id..'jg1', 'dynamic', _x, _y + 20+10+10+20, 9, 20)
      b2:newBox(ragdoll.id..'jg2', 'dynamic', _x, _y + 20+10+10+20+10+10, 8, 20)

      local head = b2:get(ragdoll.id..'head')
      local torse = b2:get(ragdoll.id..'torse')
      local jd1 = b2:get(ragdoll.id..'jd1')
      local jd2 = b2:get(ragdoll.id..'jd2')
      local jg1 = b2:get(ragdoll.id..'jg1')
      local jg2 = b2:get(ragdoll.id..'jg2')
      local bd1 = b2:get(ragdoll.id..'bd1')
      local bd2 = b2:get(ragdoll.id..'bd2')
      local bg1 = b2:get(ragdoll.id..'bg1')
      local bg2 = b2:get(ragdoll.id..'bg2')

      -- fix with a list
      head.body:setBullet(true)
      torse.body:setBullet(true)
      jd1.body:setBullet(true)
      jd2.body:setBullet(true)
      jg1.body:setBullet(true)
      jg2.body:setBullet(true)
      bd1.body:setBullet(true)
      bd2.body:setBullet(true)
      bg1.body:setBullet(true)
      bg2.body:setBullet(true)

      -- set collision groups
      head.fixture:setGroupIndex(ragdoll.groupe)
      torse.fixture:setGroupIndex(ragdoll.groupe)
      bd1.fixture:setGroupIndex(ragdoll.groupe)
      bd2.fixture:setGroupIndex(ragdoll.groupe)
      bg1.fixture:setGroupIndex(ragdoll.groupe)
      bg2.fixture:setGroupIndex(ragdoll.groupe)
      jd1.fixture:setGroupIndex(ragdoll.groupe)
      jd2.fixture:setGroupIndex(ragdoll.groupe)
      jg1.fixture:setGroupIndex(ragdoll.groupe)
      jg2.fixture:setGroupIndex(ragdoll.groupe)

      -- set all joints
      ragdoll.lstJoint["j_HeadTorse"] = love.physics.newRevoluteJoint(
        head.body, torse.body,
        head.body:getX(), head.body:getY() + 10,
        torse.body:getX(), torse.body:getY() - 20,
        false,
        math.rad(0)
      )

      ragdoll.lstJoint["j_TorseJd1"] = love.physics.newRevoluteJoint(
        torse.body, jd1.body,
        torse.body:getX(), torse.body:getY() + 20,
        jd1.body:getX(), jd1.body:getY() - 10,
        false,
        math.rad(0)
      )

      ragdoll.lstJoint["j_TorseJg1"] = love.physics.newRevoluteJoint(
        torse.body, jg1.body,
        torse.body:getX(), torse.body:getY() + 20,
        jg1.body:getX(), jg1.body:getY() - 10,
        false,
        math.rad(0)
      )

      ragdoll.lstJoint["j_Jd1Jd2"] = love.physics.newRevoluteJoint(
        jd1.body, jd2.body,
        jd1.body:getX(), jd1.body:getY() + 10,
        jd2.body:getX(), jd2.body:getY() - 10,
        false,
        math.rad(0)
      )

      ragdoll.lstJoint["j_Jg1Jg2"] = love.physics.newRevoluteJoint(
        jg1.body, jg2.body,
        jg1.body:getX(), jg1.body:getY() + 10,
        jg2.body:getX(), jg2.body:getY() - 10,
        false,
        math.rad(0)
      )

      ragdoll.lstJoint["j_TorseBd1"] = love.physics.newRevoluteJoint(
        torse.body, bd1.body,
        torse.body:getX(), torse.body:getY() - 20,
        bd1.body:getX(), bd1.body:getY() - 10,
        false,
        math.rad(0)
      )

      ragdoll.lstJoint["j_TorseBg1"] = love.physics.newRevoluteJoint(
        torse.body, bg1.body,
        torse.body:getX(), torse.body:getY() - 20,
        bg1.body:getX(), bg1.body:getY() - 10,
        false,
        math.rad(0)
      )

      ragdoll.lstJoint["j_Bd1Bd2"] = love.physics.newRevoluteJoint(
        bd1.body, bd2.body,
        bd1.body:getX(), bd1.body:getY() + 10,
        bd2.body:getX(), bd2.body:getY() - 10,
        false,
        math.rad(0)
      )

      ragdoll.lstJoint["j_Bg1Bg2"] = love.physics.newRevoluteJoint(
        bg1.body, bg2.body,
        bg1.body:getX(), bg1.body:getY() + 10,
        bg2.body:getX(), bg2.body:getY() - 10,
        false,
        math.rad(0)
      )

      -- params joints
      ragdoll.lstJoint["j_HeadTorse"]:setLimits( math.rad(-30), math.rad(30) )
      ragdoll.lstJoint["j_HeadTorse"]:setLimitsEnabled(true)
      ragdoll.lstJoint["j_TorseJd1"]:setLimits( math.rad(-120), math.rad(50) )
      ragdoll.lstJoint["j_TorseJd1"]:setLimitsEnabled(true)
      ragdoll.lstJoint["j_TorseJg1"]:setLimits( math.rad(-120), math.rad(50) )
      ragdoll.lstJoint["j_TorseJg1"]:setLimitsEnabled(true)
      ragdoll.lstJoint["j_Jd1Jd2"]:setLimits( math.rad(0), math.rad(150) )
      ragdoll.lstJoint["j_Jd1Jd2"]:setLimitsEnabled(true)
      ragdoll.lstJoint["j_Jg1Jg2"]:setLimits( math.rad(0), math.rad(150) )
      ragdoll.lstJoint["j_Jg1Jg2"]:setLimitsEnabled(true)
      ragdoll.lstJoint["j_TorseBd1"]:setLimits( math.rad(-180), math.rad(70) )
      ragdoll.lstJoint["j_TorseBd1"]:setLimitsEnabled(true)
      ragdoll.lstJoint["j_TorseBg1"]:setLimits( math.rad(-180), math.rad(70) )
      ragdoll.lstJoint["j_TorseBg1"]:setLimitsEnabled(true)
      ragdoll.lstJoint["j_Bd1Bd2"]:setLimits( math.rad(-160), math.rad(0) )
      ragdoll.lstJoint["j_Bd1Bd2"]:setLimitsEnabled(true)
      ragdoll.lstJoint["j_Bg1Bg2"]:setLimits( math.rad(-160), math.rad(0) )
      ragdoll.lstJoint["j_Bg1Bg2"]:setLimitsEnabled(true)

      -- methods
      function ragdoll:get_f(name)
        return o:get(self.id..name)
      end

      function ragdoll:get_j(name)
        return self.lstJoint[name]
      end

      function ragdoll:remove_j(name)
        self.lstJoint[name]:destroy()
      end

      return ragdoll
    end

    return o
  end
}
