# Box2DWrapper for love2D

AUTHOR                            : Damien ETHEVE

DESCRIPTION                       : A box 2D wrapper for love2D framework 

LANGUAGE                          : lua

LOVE_VERSION                      : 11.2


### Documentation

new(force, meter)                 : create a physics world

updateB2(dt)                      : update the world

drawB2()                          : draw all object in the world

get(id)                           : get an object

remove(id)                        : remove an object

newBox(id, type, x, y, w, h)      : create box 

newCircle(id, type, x, y, r)      : create circle

newPolygon(id, type, vertice)     : create new polygon

